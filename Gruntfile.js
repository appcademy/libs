module.exports = function(grunt) {
// Project configuration.
grunt.initConfig({
  pkg: grunt.file.readJSON('package.json'),
  karma:{
    unit: {
      configFile: 'karma.conf.js',
      singleRun: true,
      browsers: ['PhantomJS']
    }
  },
  jsdoc : {
        dist : {
            src: ['app/*.js', 'test/*.js'],
            options: {
                destination: 'doc',
	        configure: 'node_modules/angular-jsdoc/common/conf.json',
        	template: 'node_modules/angular-jsdoc/angular-template',
	        readme: './README.md'
            }
        }
  },
   uglify: {
    options: {
      banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
      mangle: false
    },
    build: {
      src: 'dist/<%= pkg.name %>.js',
      dest: 'dist/<%= pkg.name %>.min.js'
    }
  },
  jshint: {
      files: ['Gruntfile.js', 'app/appcademy.js', 'test/**/*.js'],
      options: {
        globals: {
          jQuery: true
        }
      }
    },
  concat: {
    options: {
      // define a string to put between each file in the concatenated output
      separator: ';'
    },
    dist: {
    // the files to concatenate
    //src: ['bower_components/baasboxSDK/baasbox.js','app/**/*.js'],
    src: ['app/**/*.js'],
    // the location of the resulting JS file
    dest: 'dist/<%= pkg.name %>.js'
  }
}
}); 
grunt.loadNpmTasks('grunt-contrib-concat');
grunt.loadNpmTasks('grunt-contrib-jshint');
grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-karma');
grunt.loadNpmTasks('grunt-jsdoc');
grunt.registerTask('default', ['jshint','jsdoc','concat','uglify']);

};
