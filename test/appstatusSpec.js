describe('module: Appcademy', function () {
    beforeEach(
        module('Appcademy')
    );

    var AppStatus;
    beforeEach(
            inject(function ($injector) {
                AppStatus = $injector.get('AppStatus');
            })
            );
    it("has a 'status' var", function () {
        expect(AppStatus.status).toBeDefined();
    });
    it('has a EXCLUDE filter', inject(function ($filter) {
        expect($filter('exclude')).not.toBeNull();
    }));

    describe('filter: EXCLUDE', function () {
        var input = [{'val': 'a', 'id': '123'}, {'val': 'b', 'id': '456'}, {'val': 'c', 'id': '789'}];
        it("should make exclusions based on full objects", inject(function (excludeFilter) {
            var res = excludeFilter(input, [input[0]]);
            expect(res[0].val).toEqual('b');
            expect(res[1].val).toEqual('c');
            expect(res[2]).not.toBeDefined();
        }));
        it("should make exclusions based on input fields", inject(function (excludeFilter) {
            var res = excludeFilter(input, ['456'], 'id');
            expect(res[0].val).toEqual('a');
            expect(res[1].val).toEqual('c');
            expect(res[2]).not.toBeDefined();
        }));
        it("should make exclusions based on fields", inject(function (excludeFilter) {
            var res = excludeFilter(input, [input[2], input[1]], 'id', 'id');
            expect(res[0].id).toEqual('123');
            expect(res[1]).not.toBeDefined();
        }));
    });
});

   