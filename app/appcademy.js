
(function () {
    'use strict';

    /**
     * @memberof Appcademy
     * @ngdoc service
     * @name AppStatus
     * @param {service} $rootScope
     * @param {service} BaasBox
     * @description 
     *   Funzioni e variabili comuni nel ciclo di vita di una app
     */
    var AppStatus = function ($rootScope, BaasBox) {



        /** 
         * Variabile che tiene lo stato dell'applicazione 
         * @memberof AppStatus
         */
        var status = {
            users: [],
            mod: {},
            more: {},
            tobesaved: {},
            filter: {},
            preedit: {},
            user: null,
            keepout: false
        };


        /**
         * Effettua il login di un utente, con funzioni di callBack in caso di esito positivo o negativo
         * @memberof AppStatus
         * @param {String} uname Nome utente
         * @param {String} pwd Password
         * @param {function} doneCallback
         * @param {function} failCallback
         */
        var login = function (uname, pwd, doneCallback, failCallback) {
            BaasBox.login(uname, pwd)
                    .done(function (loggeduser) {
                        console.log("LOGGED IN", loggeduser);
                        var er = {
                            'function': 'login',
                            'user': loggeduser,
                            'messaggio': 'login riuscito'
                        };
                        BaasBox.save(er, 'log');

                        status.user = loggeduser;
                        status.keepout = false;
                        $rootScope.$apply();
                        if (doneCallback && typeof doneCallback === "function") {
                            doneCallback(loggeduser);
                        }
                    })
                    .fail(function (err) {
                        console.log("ERROR LOGGING IN", err);
                        if (failCallback && typeof failCallback === "function") {
                            failCallback(err);
                        }
                    });
        };

        /**
         * Metodo da richiamare per attivare la procedura di recupero password
         * @memberof AppStatus
         * @param {String} username Nome utente
         */
        var forgotPassword = function (username) {
            // just a workaround, we'd better directly use username for activating reset procedure
            return BaasBox.resetPassword(username);
        };

        /**
         * Effettua il logout 
         * @memberof AppStatus
         * @param {function} callback Funzione da eseguire dopo il logout
         */
        var logout = function (callback) {
            BaasBox.logout()
                    .done(function (res) {
                        console.log("LOGGED OUT", res);
                    })
                    .fail(function (error) {
                        console.log("ERROR LOGGING OUT ", error);
                        var er = {
                            'function': 'logout',
                            'tipo': 'logout',
                            'dettaglio': error
                        };
                        BaasBox.save(er, 'errori');

                    });
            status.keepout = true;
            if (callback && typeof callback === "function") {
                callback();
            }
        };

        /**
         * Tenta di riassociare un utente già loggato, prima di richiedere le credenziali di accesso.
         * @memberof AppStatus
         * @param {function} okCallback Cosa fare se l'utente viene ricollegato (si ripristina una sessione)
         * @param {function} failCallback Cosa fare se il ripristino della sessione non funziona
         */
        var preLogin = function (okCallback, failCallback) {
            console.log("PRELOGIN 0");
            if (!status.keepout) {
                console.log("PRELOGIN");
                status.user = BaasBox.getCurrentUser();
                if (!!status.user) {
                    console.log("CURRENT USER", status.user);
                    if (okCallback && typeof okCallback === "function") {
                        okCallback(status.user);
                    }
                } else {
                    if (failCallback && typeof failCallback === "function") {
                        failCallback();
                    }
                }
            } else {
                if (failCallback && typeof failCallback === "function") {
                    failCallback();
                }
            }
        };

        /**
         * Scarica record da db
         * @memberof AppStatus
         * @param {String} collection La Collection da interrogare
         * @param {object} params Oggetto contenente i parametri di query
         * @param {function} after Cosa fare dopo aver ottenuto i dati
         * @param {String} destination OPZIONALE: nome della variabile nella quale inserire i record ottenuti
         */
        var fetch = function (collection, params, after, destination) {
            var par = params || {};
            return BaasBox.loadCollectionWithParams(collection, par)
                    .done(function (res) {
                        var dst = destination || collection;
                        status[dst] = res.data;
                        status.more[dst] = res.more;
                        $rootScope.$apply();
                        console.log(dst.toUpperCase() + " LOADED", status[dst]);
                        if (after && typeof after === "function") {
                            after(res.data[0]);
                        }
                    })
                    .fail(function (error) {
                        console.log("ERROR LOADING " + collection, error);
                        var er = {
                            'function': 'fetch',
                            'tipo': 'loadCollectionWithParams',
                            'collection': collection,
                            'parms': params,
                            'dettaglio': error
                        };
                        BaasBox.save(er, 'errori');
                    });
        };

        /**
         * Scarica da db i dati degli utenti e li inserisce nella variabile status.users
         * @memberof AppStatus
         */
        var fetchUsers = function () {
            BaasBox.fetchUsers()
                    .done(function (res) {
                        console.log("USERS LOADED", res.data);
                        status.users = res.data;
                        $rootScope.$apply();
                    })
                    .fail(function (error) {
                        console.log("ERROR LOADING USERS", error);
                        var er = {
                            'function': 'fetchUsers',
                            'tipo': 'fetchUsers',
                            'dettaglio': error
                        };
                        BaasBox.save(er, 'errori');
                    });
        };

        /**
         * Carica un dato su db o aggiorna un record
         * @memberof AppStatus
         * @param {object} item L'oggetto da caricare
         * @param {String} collection La Collection da usare
         * @param {function} after Cosa fare dopo il corretto caricamento del dato
         * @param {String} field OPZIONALE: aggiorna un record relativamente al solo campo qui specificato
         * @param {String} localcollection attingi/scrivi sulla seguente variabile locale
         */
        var insert = function (item, collection, after, field, localcollection) {
            if (!!item && !!collection) {
                if (!localcollection) {
                    localcollection = collection;
                }
                var index = status[localcollection].indexOf(item);
                if (!!field && !!item.id) {
                    return BaasBox.updateField(item.id, collection, field, descendantProp(item, field));
                    // TODO: UPDATE LOCAL OBJECT VERSION.
                } else {
                    return BaasBox.save(item, collection)
                            .done(function (res) {
                                status[localcollection][index] = res;
                                $rootScope.$apply();
                                console.log("INSERT - salvataggio avvenuto correttamente:", item);
                                if (!!after && typeof after === "function") {
                                    after(res);
                                }
                            })
                            .fail(function (error) {
                                var er = {
                                    'function': 'insert',
                                    'tipo': 'save',
                                    'collection': collection,
                                    'item': item,
                                    'field': field,
                                    'dettaglio': error
                                };
                                BaasBox.save(er, 'errori');
                                // perchè ??
                                fetch(collection);
                                alert("Non ho potuto salvare il record!\nRiprova per favore.");
                            });
                }
            }
        };

        /**
         * Rimuovi un certo elemento da una Collection
         * @memberof AppStatus
         * @param {String} collection La Collection su cui lavorare
         * @param {object} item L'elemento da eliminare
         * @param {function} after Callback opzionale
         */
        var remove = function (collection, item, after) {
            BaasBox.deleteObject(item.id, collection)
                    .success(function (res) {
                        status[collection].splice(status[collection].indexOf(item), 1);
                        console.log("APPSTATUS REMOVE - eliminato:", item);
                        $rootScope.$apply();
                        if (after && typeof after === "function") {
                            after(res[0]);
                        }
                    })
                    .error(function (err) {
                        var er = {
                            'function': 'remove',
                            'tipo': 'deleteObject',
                            'collection': collection,
                            'item': item,
                            'dettaglio': err
                        };
                        BaasBox.save(er, 'errori');
                        alert('Impossibile eliminare ' + item.nome + ' da ' + collection.toUpperCase());
                    });
        };

        /**
         * Inserisce un Link tra due record
         * @memberof AppStatus
         * @param {String} from ID del record di partenza (out)
         * @param {String} label Label da asegnare al link
         * @param {String} to ID del record di arrivo (in)
         */
        var insertLink = function (from, label, to) {
            return BaasBox.insertLink(from, label, to)
                    .fail(function (error) {
                        var er = {
                            'function': 'insertLink',
                            'tipo': 'insertLink',
                            'from': from,
                            'to': to,
                            'label': label,
                            'dettaglio': error
                        };
                        BaasBox.save(er, 'errori');
                        alert("Non ho potuto creare il link!\nRiprova per favore.");
                    });
        };

        /**
         * Ricerca un link in base ai dati inseriti
         * @memberOf AppStatus
         * @param {object} params Parametri della query
         * @returns {unresolved}
         */
        var fetchLinks = function (params) {
            return BaasBox.fetchLinks(params);
        };
        /**
         * Elimina un link, dato il suo ID
         * @memberOf AppStatus
         * @param {String} linkid ID del Link
         * @returns {unresolved}
         */
        var deleteLink = function (linkid) {
            return BaasBox.deleteLink(linkid)
                    .fail(function (error) {
                        var er = {
                            'function': 'deleteLink',
                            'tipo': 'deleteLink',
                            'id': linkid,
                            'dettaglio': error
                        };
                        BaasBox.save(er, 'errori');
                        alert("Non ho potuto cancellare il link!\nRiprova per favore.");
                    });
        };
        /**
         * Elimina un link a partire dai dati dei recordche collega
         * @param {String} outid ID del record di partenza (out)
         * @param {String} label Label assegnata al link
         * @param {String} inid ID del record di arrivo (in)
         * @param {function} after callback 
         * @returns {undefined}
         */
        var deleteLinkByData = function (outid, label, inid, after) {
            BaasBox.fetchLinks({where: "in.id='" + inid + "' AND out.id='" + outid + "' AND label='" + label + "'"})
                    .done(function (res) {
                        if (res.length > 0) {
                            BaasBox.deleteLink(res[0].id)
                                    .done(function (res) {
                                        if (!!after && typeof after === "function") {
                                            after(res);
                                        }
                                    })
                                    .fail(function (error) {
                                        var er = {
                                            'function': 'deleteLinkByData',
                                            'tipo': 'deleteLink',
                                            'in_id': inid,
                                            'out_id': outid,
                                            'label': label,
                                            'dettaglio': error
                                        };
                                        BaasBox.save(er, 'errori');
                                        alert("Non ho potuto cancellare il link!\nRiprova per favore.");
                                    });
                        }
                    })
                    .fail(function (error) {
                        var er = {
                            'function': 'deleteLinkByData',
                            'tipo': 'fetchLinks',
                            'in_id': inid,
                            'out_id': outid,
                            'label': label,
                            'dettaglio': error
                        };
                        BaasBox.save(er, 'errori');
                        alert("Non ho potuto cancellare il link!\nRiprova per favore.");
                    });
        };

        /**
         * Verifica se l'utente connesso appartiene ad un certo ruolo (gruppo di privilegi)
         * @param {String} role Nome del ruolo
         * @returns {Boolean} 
         */
        var hasRole = function (role) {
            if (status.user) {
                return (status.user.roles.indexOf(role) > -1);
            }
            return false;
        };

        /**
         * 
         * @param {type} obj
         * @param {type} is
         * @param {type} value
         * * @returns {Object}
         */
        var descendantProp = function (obj, is, value) {
            if (obj === undefined)
                return undefined;
            else if (typeof is === 'string')
                return descendantProp(obj, is.split('.'), value);
            else if (is.length === 1 && value !== undefined) {
                obj[is[0]] = value;
                return obj[is[0]];
            } else if (is.length === 0)
                return obj;
            else {
                if (value !== undefined && obj[is[0]] === undefined)
                    obj[is[0]] = {};
                return descendantProp(obj[is[0]], is.slice(1), value);
            }
        };

        /**
         * Aggiorna in batch un campo specifico dentro ad una serie di documenti
         * @param {type} collection 
         * @param {type} field Nome del campo
         * @param {type} val Nuovo valore
         * @param {type} params Parametri per la ricerca dei record
         * @param {type} after Callback opzionale
         * @returns {undefined}
         */
        var updateFieldByData = function (collection, field, val, params, after) {

            BaasBox.loadCollectionWithParams(collection, params)
                    .done(function (ress) {
                        if (ress.length > 0) {
                            console.log('APPSTATUS UPDATEFIELDBYDATA - record estratti:', ress);

                            angular.forEach(ress, function (value, key) {
                                var id = value.id;
                                BaasBox.updateField(id, collection, field, val)
                                        .done(function (res) {
                                            console.log('AGGIORNATO RECORD:', id);
                                            if (!!after && typeof after === "function") {
                                                after(res);
                                            }
                                        })
                                        .fail(function (error) {
                                            var er = {
                                                'function': 'updateFieldByData',
                                                'tipo': 'update',
                                                'collection': collection,
                                                'item': ress,
                                                'dettaglio': error
                                            };
                                            BaasBox.save(er, 'errori');
                                            alert('Impossibile eliminare ' + ress.nome + ' da ' + collection.toUpperCase());
                                            console.log('ERRORE AGGIORNATO PREZZO:', error);
                                            alert("ERRORE AGGIORNAMENTO PREZZO");
                                        });
                            });
                        }
                    })
                    .fail(function (error) {
                        var er = {
                            'function': 'updateFieldByData',
                            'tipo': 'loadCollectionWithParams',
                            'collection': collection,
                            'params': params,
                            'dettaglio': error
                        };
                        BaasBox.save(er, 'errori');
                        // perchè ??
                        console.log('APPSTATUS UPDATEFIELDBYDATA - errore:', error);
                        alert("Errore recuper dati");
                    });
        };

        /**
         * Chiama un plugin di BaasBox
         * @param {String} pluginName Nome del plugin
         * @param {String} method Metodo HTTP da usare (es. GET, POST, PUT, DELETE)
         * @param {Object} params Oggetto contenente i parametri da passare al plugin
         * @returns {unresolved}
         */
        var callPlugin = function (pluginName, method, params) {
            return BaasBox.callPlugin(pluginName, method, params);
        };

        /**
         * Abilita le notifiche push per l'utente, sul dispositivo identificato dal token
         * @param {type} os
         * @param {type} token
         * @returns {unresolved}
         */
        var enablePush = function (os, token) {
            return BaasBox.enablePush(os, token);
        };

        /**
         * Disabilita le notifiche push per il dispositivo identificato dal token
         * @param {type} token
         * @returns {unresolved}
         */
        var disablePush = function (token) {
            return BaasBox.disablePush(token);
        };

        return {
            descendantProp: descendantProp,
            login: login,
            forgotPassword: forgotPassword,
            logout: logout,
            status: status,
            preLogin: preLogin,
            fetch: fetch,
            fetchUsers: fetchUsers,
            insert: insert,
            remove: remove,
            insertLink: insertLink,
            fetchLinks: fetchLinks,
            deleteLink: deleteLink,
            deleteLinkByData: deleteLinkByData,
            hasRole: hasRole,
            updateFieldByData: updateFieldByData,
            callPlugin: callPlugin,
            enablePush: enablePush,
            disablePush: disablePush
        };
    };
    /**
     * EXCLUDE filter: elimina da un array elementi presenti in un secondo vettore
     * @memberof Appcademy
     * @ngdoc service
     * @name exclude
     * @param {array} input Array da elaborare
     * @param {array} exclude Array contenente gli elementi da eliminare da input
     * @param {String} propIn 
     * @param {String} propEx
     * @returns {unresolved}
     */
    var exclude = function () {
        return function (input, exclude, propIn, propEx) {
            if (!angular.isArray(input))
                return input;

            if (!angular.isArray(exclude))
                exclude = [];

            if (propEx) {
                exclude = exclude.map(function byProp(item) {
                    return item[propEx];
                });
            }

            return input.filter(function byExclude(item) {
                return exclude.indexOf(propIn ? item[propIn] : item) === -1;
            });
        };
    };

    angular.module('Appcademy', [])
            .constant('BaasBox', window.BaasBox)
            .factory('AppStatus', AppStatus)
            .filter('exclude', exclude);
})();
