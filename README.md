Appcademy Libs 
=====================
Librerie JS per:

- utilzzare BaasBox con Angular
- Tenere traccia di variabili di stato dell'applicazione
- Riutilizzare parti di codice che altrimenti ci troverremmo a reimplementare ogni volta...

[![Build Status](http://ci.appcademy.tech/job/APPCADEMY%20LIBS/badge/icon)](http://ci.appcademy.tech/job/APPCADEMY%20LIBS/)

DOCUMENTAZIONE
--------------
[Vedi qui](http://ci.appcademy.tech/job/APPCADEMY%20LIBS/ws/doc/Appcademy.AppStatus.html)