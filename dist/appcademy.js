
(function () {
    'use strict';

    /**
     * @memberof Appcademy
     * @ngdoc service
     * @name AppStatus
     * @param {service} $rootScope
     * @param {service} BaasBox
     * @description 
     *   Funzioni e variabili comuni nel ciclo di vita di una app
     */
    var AppStatus = function ($rootScope, BaasBox) {



        /** 
         * Variabile che tiene lo stato dell'applicazione 
         * @memberof AppStatus
         */
        var status = {
            users: [],
            mod: {},
            more: {},
            tobesaved: {},
            filter: {},
            preedit: {},
            user: null,
            keepout: false
        };


        /**
         * Effettua il login di un utente, con funzioni di callBack in caso di esito positivo o negativo
         * @memberof AppStatus
         * @param {String} uname Nome utente
         * @param {String} pwd Password
         * @param {function} doneCallback
         * @param {function} failCallback
         */
        var login = function (uname, pwd, doneCallback, failCallback) {
            BaasBox.login(uname, pwd)
                    .done(function (loggeduser) {
                        console.log("LOGGED IN", loggeduser);
                        var er = {
                                'function': 'login',
                                'user': loggeduser,
                                'messaggio': 'login riuscito'
                                };
                        BaasBox.save(er, 'log');

                        status.user = loggeduser;
                        status.keepout = false;
                        $rootScope.$apply();
                        if (doneCallback && typeof doneCallback === "function") {
                            doneCallback(loggeduser);
                        }
                    })
                    .fail(function (err) {
                        console.log("ERROR LOGGING IN", err);
                        if (failCallback && typeof failCallback === "function") {
                            failCallback(err);
                        }
                    });
        };

        /**
         * Metodo da richiamare per attivare la procedura di recupero password
         * @memberof AppStatus
         * @param {String} username Nome utente
         */
        var forgotPassword = function (username) {
            // just a workaround, we'd better directly use username for activating reset procedure
            return BaasBox.resetPassword(username);
        };

        /**
         * Effettua il logout 
         * @memberof AppStatus
         * @param {function} callback Funzione da eseguire dopo il logout
         */
        var logout = function (callback) {
            BaasBox.logout()
                    .done(function (res) {
                        console.log("LOGGED OUT", res);
                    })
                    .fail(function (error) {
                        console.log("ERROR LOGGING OUT ", error);
                        var er = {
                                    'function': 'logout',
                                    'tipo': 'logout',
                                    'dettaglio': error
                                };
                        BaasBox.save(er, 'errori');

                    });
            status.keepout = true;
            if (callback && typeof callback === "function") {
                callback();
            }
        };

        /**
         * Tenta di riassociare un utente già loggato, prima di richiedere le credenziali di accesso.
         * @memberof AppStatus
         * @param {function} okCallback Cosa fare se l'utente viene ricollegato (si ripristina una sessione)
         * @param {function} failCallback Cosa fare se il ripristino della sessione non funziona
         */
        var preLogin = function (okCallback, failCallback) {
            console.log("PRELOGIN 0");
            if (!status.keepout) {
                console.log("PRELOGIN");
                status.user = BaasBox.getCurrentUser();
                if (!!status.user) {
                    console.log("CURRENT USER", status.user);
                    if (okCallback && typeof okCallback === "function") {
                        okCallback(status.user);
                    }
                } else {
                    if (failCallback && typeof failCallback === "function") {
                        failCallback();
                    }
                }
            } else {
                if (failCallback && typeof failCallback === "function") {
                    failCallback();
                }
            }
        };

        /**
         * Scarica record da db
         * @memberof AppStatus
         * @param {String} collection La Collection da interrogare
         * @param {object} params Oggetto contenente i parametri di query
         * @param {function} after Cosa fare dopo aver ottenuto i dati
         * @param {String} destination OPZIONALE: nome della variabile nella quale inserire i record ottenuti
         */
        var fetch = function (collection, params, after, destination) {
            var par = params || {};
            return BaasBox.loadCollectionWithParams(collection, par)
                    .done(function (res) {
                        var dst = destination || collection;
                        status[dst] = res.data;
                        status.more[dst]=res.more;
                        $rootScope.$apply();
                        console.log(dst.toUpperCase() + " LOADED", status[dst]);
                        if (after && typeof after === "function") {
                            after(res.data[0]);
                        }
                    })
                    .fail(function (error) {
                        console.log("ERROR LOADING " + collection, error);
                        var er = {
                                    'function': 'fetch',
                                    'tipo': 'loadCollectionWithParams',
                                    'collection': collection,
                                    'parms': params,
                                    'dettaglio': error
                                };
                        BaasBox.save(er, 'errori');
                    });
        };

        /**
         * Scarica da db i dati degli utenti e li inserisce nella variabile status.users
         * @memberof AppStatus
         */
        var fetchUsers = function () {
            BaasBox.fetchUsers()
                    .done(function (res) {
                        console.log("USERS LOADED", res.data);
                        status.users = res.data;
                        $rootScope.$apply();
                    })
                    .fail(function (error) {
                        console.log("ERROR LOADING USERS", error);
                        var er = {
                                    'function': 'fetchUsers',
                                    'tipo': 'fetchUsers',
                                    'dettaglio': error
                                };
                        BaasBox.save(er, 'errori');
                    });
        };

        /**
         * Carica un dato su db o aggiorna un record
         * @memberof AppStatus
         * @param {object} item L'oggetto da caricare
         * @param {String} collection La Collection da usare
         * @param {function} after Cosa fare dopo il corretto caricamento del dato
         * @param {String} field OPZIONALE: aggiorna un record relativamente al solo campo qui specificato
         * @param {String} localcollection attingi/scrivi sulla seguente variabile locale
         */
        var insert = function (item, collection, after, field, localcollection) {
            if (!!item && !!collection) {
                if (!localcollection) {
                    localcollection = collection;
                }
                var index = status[localcollection].indexOf(item);
                if (!!field && !!item.id) {
                    return BaasBox.updateField(item.id, collection, field, descendantProp(item, field));
                    // TODO: UPDATE LOCAL OBJECT VERSION.
                } else {
                    return BaasBox.save(item, collection)
                            .done(function (res) {
                                status[localcollection][index] = res;
                                $rootScope.$apply();
                                console.log("INSERT - salvataggio avvenuto correttamente:",item);
                                if (!!after && typeof after === "function") {
                                    after(res);
                                }
                            })
                            .fail(function (error) {
                                var er = {
                                    'function': 'insert',
                                    'tipo': 'save',
                                    'collection': collection,
                                    'item': item,
                                    'field': field,
                                    'dettaglio': error
                                };
                                BaasBox.save(er, 'errori');
                                // perchè ??
                                fetch(collection);
                                alert("Non ho potuto salvare il record!\nRiprova per favore.");
                            });
                }
            }
        };

        /**
         * Rimuovi un certo elemento da una Collection
         * @memberof AppStatus
         * @param {String} collection La Collection su cui lavorare
         * @param {object} item L'elemento da eliminare
         */
        var remove = function (collection, item,after) {
            BaasBox.deleteObject(item.id, collection)
                    .success(function (res) {
                        status[collection].splice(status[collection].indexOf(item), 1);
                        console.log("APPSTATUS REMOVE - eliminato:",item);
                        $rootScope.$apply();
                        if (after && typeof after === "function") {
                            after(res[0]);
                        }
                    })
                    .error(function (err) {
                        var er = {
                            'function': 'remove',
                            'tipo': 'deleteObject',
                            'collection': collection,
                            'item': item,
                            'dettaglio': err
                        };
                        BaasBox.save(er, 'errori');
                        alert('Impossibile eliminare ' + item.nome + ' da ' + collection.toUpperCase());
                    });
        };

        /**
         * Inserisce un Link tra due record
         * @memberof AppStatus
         * @param {String} from ID del record di partenza (out)
         * @param {String} label Label da asegnare al link
         * @param {String} to ID del record di arrivo (in)
         */
        var insertLink = function (from, label, to) {
            return BaasBox.insertLink(from, label, to)
                    .fail(function (error) {
                        var er = {
                            'function': 'insertLink',
                            'tipo': 'insertLink',
                            'from': from,
                            'to': to,
                            'label': label,
                            'dettaglio': error
                        };
                        BaasBox.save(er, 'errori');
                        alert("Non ho potuto creare il link!\nRiprova per favore.");
                    });
        };

        /**
         * Ricerca un link in base ai dati inseriti
         * @memberOf AppStatus
         * @param {object} params Parametri della query
         * @returns {unresolved}
         */
        var fetchLinks = function (params) {
            return BaasBox.fetchLinks(params);
        };
        /**
         * Elimina un link, dato il suo ID
         * @memberOf AppStatus
         * @param {String} linkid ID del Link
         * @returns {unresolved}
         */
        var deleteLink = function (linkid) {
            return BaasBox.deleteLink(linkid)
            .fail(function (error) {
                var er = {
                    'function': 'deleteLink',
                    'tipo': 'deleteLink',
                    'id': linkid,
                    'dettaglio': error
                };
                BaasBox.save(er, 'errori');
                alert("Non ho potuto cancellare il link!\nRiprova per favore.");
            });
        };
        /**
         * Elimina un link a partire dai dati dei recordche collega
         * @param {String} outid ID del record di partenza (out)
         * @param {String} label Label assegnata al link
         * @param {String} inid ID del record di arrivo (in)
         * @param {function} after callback 
         * @returns {undefined}
         */
        var deleteLinkByData = function (outid, label, inid, after) {
            BaasBox.fetchLinks({where: "in.id='" + inid + "' AND out.id='" + outid + "' AND label='" + label + "'"})
            .done(function (res) {
                if (res.length > 0) {
                    BaasBox.deleteLink(res[0].id)
                    .done(function (res) {
                        if (!!after && typeof after === "function") {
                            after(res);
                        }
                    })
                    .fail(function (error) {
                        var er = {
                            'function': 'deleteLinkByData',
                            'tipo': 'deleteLink',
                            'in_id': inid,
                            'out_id': outid,
                            'label': label,
                            'dettaglio': error
                        };
                        BaasBox.save(er, 'errori');
                        alert("Non ho potuto cancellare il link!\nRiprova per favore.");
                    });
                }
            })
            .fail(function (error) {
                var er = {
                    'function': 'deleteLinkByData',
                    'tipo': 'fetchLinks',
                    'in_id': inid,
                    'out_id': outid,
                    'label': label,
                    'dettaglio': error
                };
                BaasBox.save(er, 'errori');
                alert("Non ho potuto cancellare il link!\nRiprova per favore.");
            });
        };

        /**
         * Verifica se l'utente connesso appartiene ad un certo ruolo (gruppo di privilegi)
         * @param {String} role Nome del ruolo
         * @returns {Boolean} 
         */
        var hasRole = function (role) {
            if (status.user) {
                return (status.user.roles.indexOf(role) > -1);
            }
            return false;
        };

        /**
         * 
         * @param {type} obj
         * @param {type} is
         * @param {type} value
         * * @returns {Object}
         */
        var descendantProp = function (obj, is, value) {
            if (obj === undefined)
                return undefined;
            else if (typeof is === 'string')
                return descendantProp(obj, is.split('.'), value);
            else if (is.length === 1 && value !== undefined) {
                obj[is[0]] = value;
                return obj[is[0]];
            } else if (is.length === 0)
                return obj;
            else {
                if (value !== undefined && obj[is[0]] === undefined)
                    obj[is[0]] = {};
                return descendantProp(obj[is[0]], is.slice(1), value);
            }
        };

        /**
         * Elimina un link a partire dai dati dei recordche collega
         * @param {String} outid ID del record di partenza (out)
         * @param {String} label Label assegnata al link
         * @param {String} inid ID del record di arrivo (in)
         * @returns {undefined}
         */
        var updateFieldByData = function (collection, field, val, params, after) {

            BaasBox.loadCollectionWithParams(collection, params)
                    .done(function (ress) {
                        if (ress.length > 0) {
                            console.log('APPSTATUS UPDATEFIELDBYDATA - record estratti:', ress);

                            angular.forEach(ress, function (value, key) {
                                var id = value.id;
                                BaasBox.updateField(id, collection, field, val)
                                .done(function (res) {
                                    console.log('AGGIORNATO RECORD:', id);
                                    if (!!after && typeof after === "function") {
                                        after(res);
                                    }
                                })
                                .fail(function (error) {
                                    var er = {
                                        'function': 'updateFieldByData',
                                        'tipo': 'update',
                                        'collection': collection,
                                        'item': ress,
                                        'dettaglio': error
                                    };
                                    BaasBox.save(er, 'errori');
                                    alert('Impossibile eliminare ' + ress.nome + ' da ' + collection.toUpperCase());
                                    console.log('ERRORE AGGIORNATO PREZZO:', error);
                                    alert("ERRORE AGGIORNAMENTO PREZZO");
                                });
                            });
                        }
                    })
                    .fail(function (error) {
                       var er = {
                           'function': 'updateFieldByData',
                           'tipo': 'loadCollectionWithParams',
                           'collection': collection,
                           'params': params,
                           'dettaglio': error
                       };
                       BaasBox.save(er, 'errori');
                       // perchè ??
                       console.log('APPSTATUS UPDATEFIELDBYDATA - errore:', error);
                       alert("Errore recuper dati");
                   });
        };

        /**
         * Chiama un plugin di BaasBox
         * @param {String} pluginName Nome del plugin
         * @param {String} method Metodo HTTP da usare (es. GET, POST, PUT, DELETE)
         * @param {Object} params Oggetto contenente i parametri da passare al plugin
         * @returns {undefined}
         */
        var callPlugin = function (pluginName, method, params) {
            return BaasBox.callPlugin(pluginName, method, params);
        };
        return {
            descendantProp: descendantProp,
            login: login,
            forgotPassword: forgotPassword,
            logout: logout,
            status: status,
            preLogin: preLogin,
            fetch: fetch,
            fetchUsers: fetchUsers,
            insert: insert,
            remove: remove,
            insertLink: insertLink,
            fetchLinks: fetchLinks,
            deleteLink: deleteLink,
            deleteLinkByData: deleteLinkByData,
            hasRole: hasRole,
            updateFieldByData: updateFieldByData,
            callPlugin: callPlugin
        };
    };
    /**
     * EXCLUDE filter: elimina da un array elementi presenti in un secondo vettore
     * @memberof Appcademy
     * @ngdoc service
     * @name exclude
     * @param {array} input Array da elaborare
     * @param {array} exclude Array contenente gli elementi da eliminare da input
     * @param {String} propIn 
     * @param {String} propEx
     * @returns {unresolved}
     */
    var exclude = function () {
        return function (input, exclude, propIn, propEx) {
            if (!angular.isArray(input))
                return input;

            if (!angular.isArray(exclude))
                exclude = [];

            if (propEx) {
                exclude = exclude.map(function byProp(item) {
                    return item[propEx];
                });
            }

            return input.filter(function byExclude(item) {
                return exclude.indexOf(propIn ? item[propIn] : item) === -1;
            });
        };
    };

    angular.module('Appcademy', [])
            .constant('BaasBox', window.BaasBox)
            .factory('AppStatus', AppStatus)
            .filter('exclude', exclude);
})();
;/*!
 * BaasBox SDK 0.9.4
 * https://github.com/baasbox/JS-SDK
 *
 * Released under the Apache license
 */

var BaasBox = (function () {

    var instance;
    var user;
    var endPoint;
    var COOKIE_KEY = "baasbox-cookie";

    // check if the user is using Zepto, otherwise the standard jQuery ajaxSetup function is executed
    if (window.Zepto) {
        $.ajaxSettings.global = true;
        $.ajaxSettings.beforeSend = function (r, settings) {
            if (BaasBox.getCurrentUser()) {
                r.setRequestHeader('X-BB-SESSION', BaasBox.getCurrentUser().token);
            }
            r.setRequestHeader('X-BAASBOX-APPCODE', BaasBox.appcode);
        };
    } else {
        $.ajaxSetup({
            global: true,
            beforeSend: function (r, settings) {
                if (BaasBox.getCurrentUser()) {
                    r.setRequestHeader('X-BB-SESSION', BaasBox.getCurrentUser().token);
                }
                r.setRequestHeader('X-BAASBOX-APPCODE', BaasBox.appcode);
            }
        });
    }

    function createInstance() {
        var object = new Object("I am the BaasBox instance");
        return object;
    }

    function setCurrentUser(userObject) {
        if (userObject === null) {
            return;
        }
        this.user = userObject;
        // if the user is using Zepto, then local storage must be used (if supported by the current browser)
        if (window.Zepto && window.localStorage) {
        //if (window.localStorage) {
            window.localStorage.setItem(COOKIE_KEY, JSON.stringify(this.user));
        } else {
            $.cookie(COOKIE_KEY, JSON.stringify(this.user));
        }
    }

    function getCurrentUser() {
        // if the user is using Zepto, then local storage must be used (if supported by the current browser)
        if (window.Zepto && window.localStorage) {
        //if (window.localStorage) {
            if (localStorage.getItem(COOKIE_KEY)) {
                this.user = JSON.parse(localStorage.getItem(COOKIE_KEY));
            }
        } else {
            if ($.cookie(COOKIE_KEY)) {
                this.user = JSON.parse($.cookie(COOKIE_KEY));
            }
        }
        return this.user;
    }

    function buildDeferred() {
        var dfd = new $.Deferred();
        var promise = {};
        promise.success = function (fn) {
            promise.then(function (data) {
                fn(data);
            });
            return promise;
        };
        promise.error = function (fn) {
            promise.then(null, function (error) {
                fn(error);
            });
            return promise;
        };

        dfd.promise(promise);
        return dfd;
    }

    return {
        appcode: "",
        pagelength: 50,
        timeout: 20000,
        version: "0.9.4",
        // permission constants
        READ_PERMISSION: "read",
        DELETE_PERMISSION: "delete",
        UPDATE_PERMISSION: "update",
        ALL_PERMISSION: "all",
        // role constants, by default in the BaasBox back end
        ANONYMOUS_ROLE: "anonymous",
        REGISTERED_ROLE: "registered",
        ADMINISTRATOR_ROLE: "administrator",
        isEmpty: function (ob) {
            for (var i in ob) {
                return false;
            }
            return true;
        },
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        },
        setEndPoint: function (endPointURL) {
            var regexp = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
            if (regexp.test(endPointURL)) {
                this.endPoint = endPointURL;
            } else {
                alert(endPointURL + " is not a valid URL");
            }
        },
        endPoint: function () {
            return this.endPoint;
        },
        login: function (user, pass) {
            var deferred = buildDeferred();
            var url = BaasBox.endPoint + '/login';
            var loginRequest = $.post(url, {
                username: user,
                password: pass,
                appcode: BaasBox.appcode
            })
                    .done(function (res) {
                        var roles = [];
                        $(res.data.user.roles).each(function (idx, r) {
                            roles.push(r.name);
                        });
                        setCurrentUser({
                            "username": res.data.user.name,
                            "token": res.data['X-BB-SESSION'],
                            "roles": roles,
                            "visibleByAnonymousUsers": res.data["visibleByAnonymousUsers"],
                            "visibleByTheUser": res.data["visibleByTheUser"],
                            "visibleByFriends": res.data["visibleByFriends"],
                            "visibleByRegisteredUsers": res.data["visibleByRegisteredUsers"]
                        });
                        deferred.resolve(getCurrentUser());
                    })
                    .fail(function (error) {
                        deferred.reject(error);
                    });
            return deferred.promise();
        },
        logout: function (cb) {
            var deferred = buildDeferred();
            var u = getCurrentUser();
            if (u === null) {
                return deferred.reject({"data": "ok", "message": "User already logged out"});
            }
            var url = BaasBox.endPoint + '/logout';
            var req = $.post(url, {})
                    .done(function (res) {
                        if (window.Zepto && window.localStorage) {
                            window.localStorage.removeItem(COOKIE_KEY);
                        } else {
                            $.cookie(COOKIE_KEY, null);
                        }
                        setCurrentUser(null);
                        deferred.resolve({"data": "ok", "message": "User logged out"})
                                .fail(function (error) {
                                    deferred.reject(error);
                                });
                    });
            return deferred.promise();
        },
        signup: function (user, pass, acl) {
            var deferred = buildDeferred();
            var url = BaasBox.endPoint + '/user';
            var postData = {username: user, password: pass};
            if (acl !== undefined || !this.isEmpty(acl)) {
                var visibilityProperties = Object.getOwnPropertyNames(acl);
                for ( var prop in acl) {
                    postData[prop] = acl[prop];
                }
            }
            var req = $.ajax({
                url: url,
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(postData)
            })
                    .done(function (res) {
                        var roles = [];
                        $(res.data.user.roles).each(function (idx, r) {
                            roles.push(r.name);
                        });
                        setCurrentUser({
                            "username": res.data.user.name,
                            "token": res.data['X-BB-SESSION'],
                            "roles": roles,
                            "visibleByAnonymousUsers": res.data["visibleByAnonymousUsers"],
                            "visibleByTheUser": res.data["visibleByTheUser"],
                            "visibleByFriends": res.data["visibleByFriends"],
                            "visibleByRegisteredUsers": res.data["visibleByRegisteredUsers"]
                        });
                        deferred.resolve(getCurrentUser());
                    })
                    .fail(function (error) {
                        deferred.reject(error);
                    });
            return deferred.promise();
        },
        signup_marty: function (user, pass, acl,scuola) {
            var deferred = buildDeferred();
            var url = BaasBox.endPoint + '/user';
            var postData = {username: user, password: pass};
            if (acl !== undefined || !this.isEmpty(acl)) {
                var visibilityProperties = Object.getOwnPropertyNames(acl);
                for ( var prop in acl) {
                    postData[prop] = acl[prop];
                }
            }
            var req = $.ajax({
                url: url,
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(postData)
            })
            .done(function (res) {
                var roles = [];
                //$(res.data.user.roles).each(function (idx, r) {
                roles.push(scuola);
                //});
                deferred.resolve(getCurrentUser());
            })
            .fail(function (error) {
                deferred.reject(error);
            });
            
            var json_role = {
                                "role":"vip",
                                "visibleByAnonymousUsers": {},
                                "visibleByTheUser": {},
                                "visibleByFriends": {},
                                "visibleByRegisteredUsers": {} 
                            }

            return deferred.promise();
        },
        getCurrentUser: function () {
            return getCurrentUser();
        },
        fetchCurrentUser: function () {
            return $.get(BaasBox.endPoint + '/me');
        },
        createCollection: function (collection) {
            return $.post(BaasBox.endPoint + '/admin/collection/' + collection);
        },
        deleteCollection: function (collection) {
            return $.ajax({
                url: BaasBox.endPoint + '/admin/collection/' + collection,
                method: 'DELETE'
            });
        },
        loadCollectionWithParams: function (collection, params) {
            var deferred = buildDeferred();
            var url = BaasBox.endPoint + '/document/' + collection;
            var req = $.ajax({
                url: url,
                method: 'GET',
                timeout: BaasBox.timeout,
                dataType: 'json',
                data: params
            })
                    .done(function (res) {
                        deferred.resolve(res);
                    })
                    .fail(function (error) {
                        deferred.reject(error);
                    });
            return deferred.promise();
        },
        loadCollection: function (collection) {
            return BaasBox.loadCollectionWithParams(collection, {page: 0, recordsPerPage: BaasBox.pagelength});
        },
        loadObject: function (collection, objectId) {
            return $.get(BaasBox.endPoint + '/document/' + collection + '/' + objectId);
        },
        save: function (object, collection) {
            var deferred = buildDeferred();
            var method = 'POST';
            var url = BaasBox.endPoint + '/document/' + collection;
            if (object.id) {
                method = 'PUT';
                url = BaasBox.endPoint + '/document/' + collection + '/' + object.id;
            }
            json = JSON.stringify(object);
            var req = $.ajax({
                url: url,
                type: method,
                contentType: 'application/json',
                dataType: 'json',
                data: json
            })
                    .done(function (res) {
                        deferred.resolve(res['data']);
                    })
                    .fail(function (error) {
                        deferred.reject(error);
                    });
            return deferred.promise();
        },
        updateField: function (objectId, collection, fieldName, newValue) {
            var deferred = buildDeferred();
            
            var props = fieldName.replace(/\./g, '/\.');
            url = BaasBox.endPoint + '/document/' + collection + '/' + objectId + '/.' + props;
            var json = JSON.stringify({
                "data": newValue
            });
            var req = $.ajax({
                url: url,
                type: 'PUT',
                contentType: 'application/json',
                dataType: 'json',
                data: json
            })
                    .done(function (res) {
                        deferred.resolve(res['data']);
                    })
                    .fail(function (error) {
                        deferred.reject(error);
                    });
            return deferred.promise();
        },
        updateObject: function (objectId, collection, newData) {
            var deferred = buildDeferred();
            url = BaasBox.endPoint + '/document/' + collection + '/' + objectId;
            var req = $.ajax({
                url: url,
                type: 'PUT',
                contentType: 'application/json',
                dataType: 'json',
                data: JSON.stringify(newData)
            })
                    .done(function (res) {
                        deferred.resolve(res['data']);
                    })
                    .fail(function (error) {
                        deferred.reject(error);
                    });
            return deferred.promise();
        },
        deleteObject: function (objectId, collection) {
            return $.ajax({
                url: BaasBox.endPoint + '/document/' + collection + '/' + objectId,
                method: 'DELETE'
            });
        },
        fetchObjectsCount: function (collection) {
            return $.get(BaasBox.endPoint + '/document/' + collection + '/count');
        },
        grantUserAccessToObject: function (collection, objectId, permission, username) {
            return $.ajax({
                url: BaasBox.endPoint + '/document/' + collection + '/' + objectId + '/' + permission + '/user/' + username,
                method: 'PUT'
            });
        },
        revokeUserAccessToObject: function (collection, objectId, permission, username) {
            return $.ajax({
                url: BaasBox.endPoint + '/document/' + collection + '/' + objectId + '/' + permission + '/user/' + username,
                method: 'DELETE'
            });
        },
        grantRoleAccessToObject: function (collection, objectId, permission, role) {
            return $.ajax({
                url: BaasBox.endPoint + '/document/' + collection + '/' + objectId + '/' + permission + '/role/' + role,
                method: 'PUT'
            });
        },
        revokeRoleAccessToObject: function (collection, objectId, permission, role) {
            return $.ajax({
                url: BaasBox.endPoint + '/document/' + collection + '/' + objectId + '/' + permission + '/role/' + role,
                method: 'DELETE'
            });
        },
        // only for json assets
        loadAssetData: function (assetName) {
            var deferred = buildDeferred();
            var url = BaasBox.endPoint + '/asset/' + assetName + '/data';
            var req = $.ajax({
                url: url,
                method: 'GET',
                contentType: 'application/json',
                dataType: 'json'
            })
                    .done(function (res) {
                        deferred.resolve(res['data']);
                    })
                    .fail(function (error) {
                        deferred.reject(error);
                    });
            return deferred.promise();
        },
        getImageURI: function (name, params) {
            var deferred = buildDeferred();
            var uri = BaasBox.endPoint + '/asset/' + name;
            var r;
            if (params === null || this.isEmpty(params)) {
                return deferred.resolve({"data": uri + "?X-BAASBOX-APPCODE=" + BaasBox.appcode});
            }
            for (var prop in params) {
                var a = [];
                a.push(prop);
                a.push(params[prop]);
                r = a.join('/');
            }
            uri = uri.concat('/');
            uri = uri.concat(r);
            p = {};
            p['X-BAASBOX-APPCODE'] = BaasBox.appcode;
            var req = $.get(uri, p)
                    .done(function (res) {
                        deferred.resolve({"data": this.url});
                    })
                    .fail(function (error) {
                        (error);
                    });
            return deferred.promise();
        },
        fetchUserProfile: function (username) {
            return $.get(BaasBox.endPoint + '/user/' + username);
        },
        fetchUsers: function (params) {
            return $.ajax({
                url: BaasBox.endPoint + '/users',
                method: 'GET',
                data: params
            });
        },
        updateUserProfile: function (params) {
            return $.ajax({
                url: BaasBox.endPoint + '/me',
                method: 'PUT',
                contentType: 'application/json',
                data: JSON.stringify(params)
            });
        },
        changePassword: function (oldPassword, newPassword) {
            return $.ajax({
                url: BaasBox.endPoint + '/me/password',
                method: 'PUT',
                contentType: 'application/json',
                data: JSON.stringify({old: oldPassword, new : newPassword})
            });
        },
        resetPassword: function (username) {
            return $.get(BaasBox.endPoint + '/user/' + username + '/password/reset');
        },
        followUser: function (username) {
            return $.post(BaasBox.endPoint + '/follow/' + username);
        },
        unfollowUser: function (username) {
            return $.ajax({
                url: BaasBox.endPoint + '/follow/' + username,
                method: 'DELETE'
            });
        },
        fetchFollowers: function (username) {
            return $.get(BaasBox.endPoint + '/followers/' + username);
        },
        fetchFollowing: function (username) {
            return $.get(BaasBox.endPoint + '/following/' + username);
        },
        sendPushNotification: function (params) {
            return $.ajax({
                url: BaasBox.endPoint + '/push/message',
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(params)
            });
        },
        uploadFile: function (formData) {
            return $.ajax({
                url: BaasBox.endPoint + '/file',
                type: 'POST',
                data: formData,
                mimeType: "multipart/form-data",
                contentType: false,
                cache: false,
                processData: false
            });
        },
        fetchFile: function (fileId) {
            return $.get(BaasBox.endPoint + '/file/' + fileId + "?X-BB-SESSION=" + BaasBox.getCurrentUser().token);
        },
        deleteFile: function (fileId) {
            return $.ajax({
                url: BaasBox.endPoint + '/file/' + fileId,
                method: 'DELETE'
            });
        },
        fetchFileDetails: function (fileId) {
            return $.get(BaasBox.endPoint + '/file/details/' + fileId);
        },
        grantUserAccessToFile: function (fileId, permission, username) {
            return $.ajax({
                url: BaasBox.endPoint + '/file/' + fileId + '/' + permission + '/user/' + username,
                method: 'PUT'
            });
        },
        revokeUserAccessToFile: function (fileId, permission, username) {
            return $.ajax({
                url: BaasBox.endPoint + '/file/' + fileId + '/' + permission + '/user/' + username,
                method: 'DELETE'
            });
        },
        grantRoleAccessToFile: function (fileId, permission, rolename) {
            return $.ajax({
                url: BaasBox.endPoint + '/file/' + fileId + '/' + permission + '/role/' + rolename,
                method: 'PUT'
            });
        },
        revokeRoleAccessToFile: function (fileId, permission, rolename) {
            return $.ajax({
                url: BaasBox.endPoint + '/file/' + fileId + '/' + permission + '/role/' + rolename,
                method: 'DELETE'
            });
        },
        /* API for calling a plugin with params */
        callPlugin: function (pluginName, method, data) {
            var deferred = buildDeferred();
            var req = $.ajax({
                url: BaasBox.endPoint + '/plugin/' + pluginName,
                method: method.toLowerCase(),
                data: JSON.stringify(data),
                contentType: 'application/json',
                dataType: 'json'
            })
                    .done(function (res) {
                        deferred.resolve(res);
                    })
                    .fail(function (error) {
                        deferred.reject(error);
                    });
            return deferred.promise();
        },
        
        /* LINK API */
        insertLink: function (outID, label, inID) {
            var deferred = buildDeferred();
            var req = $.ajax({
                url: BaasBox.endPoint + '/link/' + outID + '/' + label + '/' + inID,
                method: 'POST'
            })
                    .done(function (res) {
                        deferred.resolve(res);
                    })
                    .fail(function (error) {
                        deferred.reject(error);
                    });
            return deferred.promise();
        },
        fetchLinks: function (params) {
            var deferred = buildDeferred();
            var url = BaasBox.endPoint + '/link';
            var req = $.ajax({
                url: url,
                method: 'GET',
                timeout: BaasBox.timeout,
                dataType: 'json',
                data: params
            })
                    .done(function (res) {
                        deferred.resolve(res['data']);
                    })
                    .fail(function (error) {
                        deferred.reject(error);
                    });
            return deferred.promise();
        },
        deleteLink: function (linkid) {
            return $.ajax({
                url: BaasBox.endPoint + '/link/' + linkid,
                method: 'DELETE'
            });
        }
    };
})();

/*!
 * jQuery Cookie Plugin v1.4.0
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as anonymous module.
        define(['jquery'], factory);
    } else {
        // Browser globals.
        factory(jQuery);
    }
}(function ($) {

    var pluses = /\+/g;

    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }

    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }

    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }

    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape...
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }

        try {
            // Replace server-side written pluses with spaces.
            // If we can't decode the cookie, ignore it, it's unusable.
            // If we can't parse the cookie, ignore it, it's unusable.
            s = decodeURIComponent(s.replace(pluses, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch (e) {
        }
    }

    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }

    var config = $.cookie = function (key, value, options) {

        // Write
        if (value !== undefined && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);

            if (typeof options.expires === 'number') {
                var days = options.expires,
                        t = options.expires = new Date();
                t.setDate(t.getDate() + days);
            }

            return (document.cookie = [
                encode(key), '=', stringifyCookieValue(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path ? '; path=' + options.path : '',
                options.domain ? '; domain=' + options.domain : '',
                options.secure ? '; secure' : ''
            ].join(''));
        }

        // Read

        var result = key ? undefined : {};

        // To prevent the for loop in the first place assign an empty array
        // in case there are no cookies at all. Also prevents odd result when
        // calling $.cookie().
        var cookies = document.cookie ? document.cookie.split('; ') : [];

        for (var i = 0, l = cookies.length; i < l; i++) {
            var parts = cookies[i].split('=');
            var name = decode(parts.shift());
            var cookie = parts.join('=');

            if (key && key === name) {
                // If second argument (value) is a function it's a converter...
                result = read(cookie, value);
                break;
            }

            // Prevent storing a cookie that we couldn't decode.
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }

        return result;
    };

    config.defaults = {};

    $.removeCookie = function (key, options) {
        if ($.cookie(key) === undefined) {
            return false;
        }

        // Must not alter options, thus extending a fresh object...
        $.cookie(key, '', $.extend({}, options, {
            expires: -1
        }));
        return !$.cookie(key);
    };

}));